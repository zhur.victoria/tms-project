from main_page import MainPage


def test_title(driver):
    main_page = MainPage(driver)
    main_page.open_page()
    assert 'Oscar - Sandbox' == driver.title, 'Wrong tab'


def test_page_url(driver):
    main_page = MainPage(driver)
    main_page.open_page()
    assert 'http://selenium1py.pythonanywhere.com/en-gb/' ==\
           driver.current_url, 'Wrong URL'
