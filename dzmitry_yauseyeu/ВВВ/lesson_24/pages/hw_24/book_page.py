from base_page_hw import BasePage
from books_locators import BookPageLocators


class BookPage(BasePage):
    URL = "http://selenium1py.pythonanywhere.com/en-gb"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def open_books(self):
        books = self.driver.find_element(*BookPageLocators.BOOKS_LINK)
        books.click()


class SelectBookPage(BookPage):
    def select_book(self):
        offers = self.driver.find_element(*BookPageLocators.first_book)
        offers.click()
        assert BookPageLocators.selected_book_idicator \
               == BookPageLocators.selected_book_idicator, "Incorrect" \
                                                           " location page"

    def find_book_by_name(self, title):
        list_of_books = self.driver.find_elements(
            *BookPageLocators.Seeker_OF_BOOKS)
        for book in list_of_books:
            if book.text == title:
                book.click()
                return BookPage(self.driver)
