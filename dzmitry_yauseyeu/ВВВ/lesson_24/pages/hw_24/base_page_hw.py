from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class BasePage:

    def __init__(self, driver, url):
        self.driver = driver
        self.url = url

    def open_page(self):
        self.driver.get(self.url)

    def find_element(self, locator, timeout=10):
        return WebDriverWait(self.driver, timeout).until(
            ec.presence_of_element_located(locator)
        )

    def element_is_clickable(self, locator, timeout=10):
        return WebDriverWait(self.driver, timeout).until(
            ec.element_to_be_clickable(locator))
