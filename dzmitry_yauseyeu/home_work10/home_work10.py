from statistics import mean
"""
Цветочница.

Определить иерархию и создать несколько цветов
(Rose, Tulips, violet,
chamomile). У каждого класса цветка следующие атрибуты:
Стоимость – атрибут класса, который определяется заранее
Свежесть (в днях), цвет, длинна стебля – атрибуты экземпляра
При попытке вывести информацию о цветке,
должен отображаться цвет и тип
Rose = RoseClass(7, Red, 15)
Print(Rose) -> Red Rose (__str__)
Собрать букет (можно использовать
аксессуары – отдельные классы со своей
стоимостью: например, упаковочная бумага)
с определением ее стоимости.
У букета должна быть возможность
определить время его увядания по среднему
времени жизни всех цветов в букете
Позволить сортировку цветов в букете на основе различных параметров
(свежесть/цвет/длина стебля/стоимость...)
Реализовать поиск цветов в букете по определенным параметрам.
Узнать, есть ли цветок в букете. (__contains__)
Добавить возможность получения цветка по
индексу (__getitem__) либо возможность
пройтись по букету и получить каждый
цветок по-отдельности (__iter__)
"""


class Flowers:
    def __init__(self, freshness, color, len_of_stick, name):
        self.freshness = freshness
        self.color = color
        self.len_of_stick = len_of_stick
        self.name = name


class Rose(Flowers):
    price = 11

    def __str__(self):
        return f"{self.color} {Rose.__name__}"


rose = Rose(7, "Red", 15, "Rose")
print(rose)


class Tulips(Flowers):
    price = 6

    def __str__(self):
        return f"{self.color} {Tulips.__name__}"


tulips = Tulips(8, "Blue", 11, "Tulips")
print(tulips)


class Violet(Flowers):
    price = 7

    def __str__(self):
        return f"{self.color} {Violet.__name__}"


violet = Violet(9, "Purple", 13, "Violet")
print(violet)


class Chamomile(Flowers):
    price = 8

    def __str__(self):
        return f"{self.color} {Chamomile.__name__}"


chamomile = Chamomile(6, "Orange", 7, "Chamomile")
print(chamomile)

# Узнать, есть ли цветок в букете. (__contains__)
# Добавить возможность получения цветка по индексу
# (__getitem__) либо
# возможность пройтись по букету и получить каждый
# цветок
# по-отдельности (__iter__)


class Accessories:
    def __init__(self, color):
        self.color = color


class Paper(Accessories):
    price = 11

    def __init__(self, color, page_size="A4"):
        super().__init__(color)
        self.page_size = page_size


paper = Paper("blue", "A3")
print("Paper color is {}, pageSize is {}"
      .format(paper.color, paper.page_size))


class Bouquet:

    def __init__(self, flowers, accessories):
        self.flowers = flowers
        self.accessories = accessories

    def __getitem__(self, index):
        return self.flowers[index]

    def __contains__(self, obj):
        return obj in self.flowers

    def __iter__(self):
        return iter(self.flowers)

    # Собрать букет (можно использовать
    # аксессуары – отдельные классы со своей
    # стоимостью: например, упаковочная бумага)
    # с определением ее стоимости.

    @property
    def bouquet_price(self):
        bouquet_price_sum = sum([arg.price for arg in self.flowers])
        bouquet_acc_sum = sum([arg.price for arg in self.accessories])
        return bouquet_price_sum + bouquet_acc_sum

    # У букета должна быть возможность определить время его увядания по
    # среднему времени жизни всех цветов в букете

    def bouquet_term(self):
        bouquet_term = mean([arg.freshness for arg in self.flowers])
        return bouquet_term

    # Позволить сортировку цветов в букете на основе различных параметров
    # (свежесть/цвет/длина стебля/стоимость...)

    def bouquet_sort(self, param):
        full_param_name = ""

        if param == "f":
            full_param_name = "freshness"
        if param == "c":
            full_param_name = "color"
        if param == "l":
            full_param_name = "len of stick"
        if param == "p":
            full_param_name = "price"

        print("Sort by {}".format(full_param_name))

        if param == "f":
            self.bouquet_sort_freshness()
        if param == "c":
            self.bouquet_sort_color()
        if param == "l":
            self.bouquet_sort_len_of_stick()
        if param == "p":
            self.bouquet_sort_price()

        print("")

    def bouquet_sort_freshness(self):
        new_list = sorted(self.flowers,
                          key=lambda x: x.freshness, reverse=True)

        for i in new_list:
            print(i.name, i.freshness)
            pass

    def bouquet_sort_color(self):

        new_list = sorted(self.flowers,
                          key=lambda x: x.color, reverse=True)

        for i in new_list:
            print(i.name, i.color)
            pass

    def bouquet_sort_len_of_stick(self):
        new_list_2 = sorted(self.flowers,
                            key=lambda x: x.len_of_stick, reverse=True)

        for i in new_list_2:
            print(i.name, i.len_of_stick)
            pass

    def bouquet_sort_price(self):

        new_list_3 = sorted(self.flowers,
                            key=lambda x: x.price, reverse=True)

        for i in new_list_3:
            print(i.name, i.price)
            pass
    # Реализовать поиск цветов в букете по определенным параметрам.

    def bouquet_search(self, freshness=None,
                       color=None, len_of_stick=None, name=None):
        result_list = []
        for i in self.flowers:
            if not freshness or freshness <= i.freshness:
                if not color or color == i.color:
                    if not len_of_stick or \
                            len_of_stick >= i.len_of_stick:
                        if not name or name == i.name:
                            result_list.append(i)
        return result_list

    def show(self):
        Bouquet.print(self.flowers)

    @staticmethod
    def print(flowers_list):
        print("The bouquet consists of")
        for flower in flowers_list:
            print(flower.name)
        print("")


american_rose = Rose(2, "Red", 15, "American Rose")

bouquet = Bouquet([rose, rose, american_rose,
                   tulips, chamomile], [paper])
bouquet.show()

print("Bouquet price is {}".format(bouquet.bouquet_price))
print("Bouquet term is {}".format(bouquet.bouquet_term()))

bouquet.bouquet_sort("f")
bouquet.bouquet_sort("c")
bouquet.bouquet_sort("l")
bouquet.bouquet_sort("p")

Bouquet.print(bouquet.bouquet_search(7))
Bouquet.print(bouquet.bouquet_search(1, "Red"))
Bouquet.print(bouquet.bouquet_search(23, "Red", 3, 2))
