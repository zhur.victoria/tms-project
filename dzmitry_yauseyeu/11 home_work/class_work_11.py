# from abc import ABC, abstractmethod
#
# """
# Методы класса Animal:
#     Идти
#     Спать
#     Есть
#     пить
# Pet:
#     любить
#     есть_тапки
#
# WildAnimal:
#     охотится
#     рыть_яму
#     рычать
# """
#
#
# class Animal(ABC):
#     @abstractmethod
#     def go(self):
#         print("animal is beginning to go")
#
#     @abstractmethod
#     def sleep(self):
#         print("animal fall in sleep ")
#
#     @abstractmethod
#     def eat(self):
#         print("animal is beginning to eat")
#
#     @abstractmethod
#     def drink(self):
#         print("animal is beginning to drink")
#
#
# class Pet(Animal):
#     @abstractmethod
#     def love(self):
#         print("Pet fall in love")
#
#
# class Cat(Pet):
#
#     def eat(self):
#         print("Cat is beginning to eat a slippers ")
#
#     def go(self):
#         print("Cat is beginning to eat")
#
#     def sleep(self):
#         print("Cat is beginning to eat")
#
#     def drink(self):
#         print("Cat is beginning to drink")
#
#     def love(self):
#         print("Cat fall in love")
#
#
# class WildAnimal(Animal):
#     @abstractmethod
#     def rrrr(self):
#         print("WildAnimal is beginning to Rrrrr")
#
#     @abstractmethod
#     def dig(self):
#         print("WildAnimal is beginning to Dig a hole")
#
#
# class Lion(WildAnimal):
#
#     @abstractmethod
#     def eat(self):
#         print("Lion is beginning to eat a slippers ")
#
#     @abstractmethod
#     def go(self):
#         print("Lion is beginning to eat")
#
#     @abstractmethod
#     def sleep(self):
#         print("Lion is beginning to eat")
#
#     @abstractmethod
#     def drink(self):
#         print("Lion is beginning to drink")
#
#     @abstractmethod
#     def rrrr(self):
#         print("Lion is beginning to Rrrrr")
#
#     @abstractmethod
#     def dig(self):
#         print("Lion is beginning to Dig a hole")
#
#
# cat1 = Cat()
# cat1.love()
# Lion1 = Lion()
# Lion1.rrrr()

"""Создать класс Book. Атрибуты: количество страниц,
 год издания, автор, цена. Добавить валидацию в конструкторе
  на ввод корректных данных. Создать иерархию ошибок."""


class ErrorPages(Exception):
    message = "error pages"

    def __init__(self):
        super().__init__(self.message)


class ErrorYear(Exception):
    message = "error pages"

    def __init__(self):
        super().__init__(self.message)


class ErrorAutor(Exception):
    message = "error pages"

    def __init__(self):
        super().__init__(self.message)


class ErrorPrice(Exception):
    message = "error price"

    def __init__(self):
        super().__init__(self.message)


class Book(Exception):
    def __init__(self, count_pages, year_book, autor, price):
        self.count_pages = self.validator_pages(count_pages)
        self.year_book = self.validator_year(year_book)
        self.autor = self.valid_author(autor)
        self.price = self.valid_price(price)

    @staticmethod
    def validator_pages(count_pages):

        if count_pages > 400:
            raise ErrorPages
        return count_pages

    @staticmethod
    def validator_year(year_book):

        if year_book < 1980:
            raise ErrorYear
        return year_book

    @staticmethod
    def valid_author(author):
        if not author.isalpha():
            raise ErrorAutor
        return author

    @staticmethod
    def valid_price(price):
        if not 100 < price < 10000:
            raise ErrorPrice
        return price


book1 = Book(300, 1980, "DIMA", 4000)
print(book1)
print(book1.validator_pages(11))

# """
# Создать lambda функцию, которая принимает на вход имя и выводит его в формате
# “Hello, {name}”
# """
#
# """func = lambda nam: f"Hello, {name}"
#     print(func("dima"))"""
#
# """
# Создать lambda функцию, которая принимает на вход список имен и выводит их
# в формате “Hello, {name}” в другой список
# """

# name = ["Dima", "Max"]
#
# # func = lambda nam: [f"{x}" for x in name]
# # print(func(name))
#
# func = map(lambda y: f"{y}", name)
# print(list(func))
#
# """
# Дан список чисел. Вернуть список, где каждый число переведено в строку
# [5, 3] -> [‘5’, ‘3’]
# """
# x = [5, 3]
# print(list(map(lambda k: f"{k}", x)))
#
# """
# Дан список имен, отфильтровать все имена, где есть буква k
# """
# filter_name = list(filter(lambda i: "k" in i, name))
# print(filter_name)
#
# """
# отсортировать словарь с помощью функции sorted()
# """
# s = {"a": 10, "b": 20, "c": 0, "d": -1}
# print({k: v for k, v in sorted(s.items(), key=lambda item: item[1])})
