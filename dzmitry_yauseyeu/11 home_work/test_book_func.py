from unittest import TestCase
from class_work_11 import\
    Book, ErrorPages, ErrorYear, ErrorAutor, ErrorPrice
import unittest
from unittest import mock


def mock_validate_author_of_name(author):
    return author


class TestBook(TestCase):
    def test_validate_author_name_positive(self):
        self.book = Book(200, 1980, 'Dima', 500)
        self.assertEqual(Book, Book)

    def test_validate_author_name_via_error_author(self):
        with self.assertRaises(ErrorAutor) as error_of_Autor:
            self.book = Book(200, 1980, 'Dima1', 500)
        self.assertEqual(str(error_of_Autor.exception), 'error pages')

    def test_validate_author_name_special_chars_via_error_author(self):
        with self.assertRaises(ErrorAutor) as error_of_Autor:
            self.book = Book(200, 1980, 'Dima-DIma', 500)
        self.assertEqual(
            str(error_of_Autor.exception), 'error pages')

    def test_validate_year_too_early(self):
        with self.assertRaises(ErrorYear) as error_of_year:
            self.book = Book(200, 1979, 'Dima', 500)
        self.assertEqual(
            str(error_of_year.exception), 'error pages')

    def test_validate_pages_many(self):
        with self.assertRaises(ErrorPages) as error_of_page:
            self.book = Book(20012, 1980, 'Dima', 500)
        self.assertEqual(
            str(error_of_page.exception), 'error pages')

    def test_validate_price_low(self):
        with self.assertRaises(ErrorPrice) as error_of_price:
            self.book = Book(200, 1980, 'Dima', 99.99)
        self.assertEqual(
            error_of_price.exception.message, 'error price')

    def test_validate_price_high(self):
        with self.assertRaises(ErrorPrice) as error_of_price:
            self.book = Book(200, 1980, 'Dima', 99999.01)
        self.assertEqual(
            str(error_of_price.exception), 'error price')

    @mock.patch('class_work_11.Book.valid_author',
                side_effect=mock_validate_author_of_name)
    def test_validate_author_name_digit_7(self, valid_author):
        self.book = Book(200, 1980, 'Dima', 999.01)
        self.assertEqual(valid_author('Dima7'), 'Dima7')

    @unittest.expectedFailure
    def test_author_name_with_digits(self):
        self.assertEqual(Book.valid_author('444'), False)
