"""
проверяет что все буквы в переданном слове в нижнем регистре и возвращает
True либо False
"""


def check_lower_case(arg):
    return arg.islower()
