import math
from statistics import mean, geometric_mean


"""Task#1: Даны 2 действительных числа a и b. Получить их сумму, разность и
произведение."""

a = 5
b = 4
summ = a + b  # sum of a and b
difference = a - b  # difference of a and b
product = a * b  # product of a and b
print(summ, difference, product)


"""Task#2: Даны действительные числа x и y. Получить |x|-|y|/1+|xy|."""

x = -5
y = 2
z = (abs(x) - abs(y)) / (1 + abs(x * y))  # z=|x|-|y|/1+|xy|
print(z)


"""Task#3: Дана длина ребра куба. Найти объем куба и площадь его боковой
поверхности."""

edge = 5  # edge length
volume = edge ** 3  # volume of cube
lateral_area = 4 * edge ** 2  # lateral surface area of cube
print(volume, lateral_area)


"""Task#4: Даны два действительных числа. Найти среднее арифметическое и
среднее геометрическое этих чисел."""

numbers = [2, 3]  # input numbers
arithmetic_avg = mean(numbers)  # arithmetic mean
geometric_avg = geometric_mean(numbers)  # geometric mean
print(arithmetic_avg, geometric_avg)


"""Task#5: Даны катеты прямоугольного треугольника. Найти его гипотенузу и
площадь."""

leg_1 = 3  # 1st leg of triangle
leg_2 = 4  # 2nd leg of triangle
hypotenuse = math.sqrt(leg_1 ** 2 + leg_2 ** 2)  # get of hypotenuse by legs
print(hypotenuse)
