from bank_func import deposit_calculation
import unittest


class TestDepositCalculation(unittest.TestCase):
    def test_deposit_is_0(self):
        self.assertEqual(deposit_calculation(0, 10), 0)

    def test_period_is_less_than_year(self):
        self.assertEqual(deposit_calculation(100, 0), 100)

    def test_regular_deposit(self):
        self.assertEqual(deposit_calculation(100, 10), 259.37424601)

    def test_initial_deposit_is_negative(self):
        self.assertNotEqual(deposit_calculation(-100, 10), 259.37424601)

    @unittest.expectedFailure
    def test_period_is_float(self):
        self.assertEqual(deposit_calculation(100, 1.1), 110)


if __name__ == '__main__':
    unittest.main()
