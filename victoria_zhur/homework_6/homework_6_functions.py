from functools import reduce


"""1.Validate. Ваша задача написать программу, принимающее число - номер
кредитной карты(число может быть четным или не четным).
И проверяющей может ли такая карта существовать.
Предусмотреть защиту от ввода букв, пустой строки и т.д."""

cc_to_check = input("Enter CC number to validate: ")
cc_to_check = cc_to_check.replace(" ", "")


def luhn_validation(cc_to_check: str) -> bool:
    """
    This function validates credit card by Luhn algorithm
    """
    lookup = (0, 2, 4, 6, 8, 1, 3, 5, 7, 9)
    cc_to_check = reduce(str.__add__, filter(str.isdigit, cc_to_check))
    evens = sum(int(i) for i in cc_to_check[-1::-2])
    odds = sum(lookup[int(i)] for i in cc_to_check[-2::-2])
    return (evens + odds) % 10 == 0


print(luhn_validation(cc_to_check))


"""2. Подсчет количества букв
На вход подается строка, например, "cccbba" результат работы программы -
строка 'c3b2a1'"""

input_string = input("Enter string to count chars: ")


def char_count(input_string: str) -> str:
    """
    This function counts amount of unique chars in input string
    """
    counted_string = []
    for char in input_string:
        c = char + str(input_string.count(char))
        if c not in counted_string:
            counted_string.append(c)
    counted_string = "".join(counted_string)
    return counted_string


print(char_count(input_string))


"""3. Простейший калькулятор v0.1. Реализуйте программу, которая спрашивала
у пользователя, какую операцию он хочет произвести над числами, а затем
запрашивает два числа и выводит результат. Проверка деления на 0."""

act = int(input("Select the operation: 1) summ, 2) substraction,"
                " 3) multiplication, 4) division: "))
first_number = float(input("Enter the first number: "))
second_number = float(input("Enter the second number: "))


def calculator(act: int, first_number: float, second_number: float) -> float:
    """
    This is a simple calculator
    """
    if act == 1:
        return first_number + second_number
    elif act == 2:
        return first_number - second_number
    elif act == 3:
        return first_number * second_number
    elif act == 4 and second_number != 0:
        return first_number / second_number
    else:
        pass


print(calculator(act, first_number, second_number))


"""4.Написать функцию с изменяемым числом входных параметров."""


def func_4(x, *args, name=None, **kwargs):
    """
    This function returns different types of arguments
    """
    return {"mandatory_position_argument": x,
            "additional_position_arguments": args,
            "mandatory_named_argument": {'name': name},
            "additional_named_arguments": {**kwargs}
            }


print(func_4(1, 2, 3, name="test", surname="test2", some="something"))


"""5. Работа с областями видимости. На уровне модуля создать список из 3-х
элементов. Написать функцию, которая принимает на вход этот список и добавляет
в него элементы. Функция должна вернуть измененный список.
При этом исходный список не должен измениться."""

my_list = [1, 2, 3]


def change_list(my_list: list) -> list:
    """
    This function adds element "a" to the end of the list
    """
    changed_list = my_list[:]
    changed_list.append("a")
    return changed_list


print(my_list)
print(change_list(my_list))


"""6. Функция проверяющая тип данных. Написать функцию которая принимает на
вход список из чисел, строк и таплов.
Функция должна вернуть сколько в списке элементов приведенных данных
print(my_function([1, 2, “a”, (1, 2), “b”]) ->
{“int”: 2, “str”: 2, “tuple”: 1}"""


def check_type(list_of_args: list):
    """
    This function checks and counts type of input data
    """
    int_number = 0
    str_number = 0
    tuple_number = 0
    for i in range(len(list_of_args)):
        if isinstance(list_of_args[i], int):
            int_number += 1
        elif isinstance(list_of_args[i], str):
            str_number += 1
        elif isinstance(list_of_args[i], tuple):
            tuple_number += 1
    return {"int": int_number, "str": str_number, "tuple": tuple_number}


list_of_args = [1, 2, "a", (1, 2), "b"]
print(check_type(list_of_args))


"""7. Написать пример чтобы hash от объекта 1 и 2 были одинаковые,
а id разные."""

first_number = 1
second_number = 1.0
print(id(first_number), id(second_number))
print(hash(first_number), hash(second_number))


"""8. Написать функцию, которая проверяет есть ли в списке объект,
которы можно вызвать"""


def whether_callable(list_to_check: list):
    """
    This function checks whether callable object exists in the list (-> True)
    or not (-> False).
    """
    for obj in list_to_check:
        if callable(obj) is True:
            return True
    return False


list_to_check = [False, int, 123, "string"]
print(whether_callable(list_to_check))
