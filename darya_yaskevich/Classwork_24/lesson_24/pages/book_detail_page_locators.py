from selenium.webdriver.common.by import By


class BookDetailPageLocators:
    BOOK_NAME = (By.XPATH, '//h1')
    BOOK_IMAGE = (By.CSS_SELECTOR, '.item.active > img')
