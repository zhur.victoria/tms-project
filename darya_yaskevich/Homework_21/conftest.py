from selenium import webdriver
import pytest


@pytest.fixture
def driver():
    driver = webdriver.Chrome('C:\\data\\chromedriver_win32\\chromedriver.exe')
    yield driver
    driver.quit()


@pytest.fixture
def open_main_page(driver):
    driver.get("http://the-internet.herokuapp.com/")
    return driver
