# # автор не должен содержать других символов кроме букв
# # год не < 1980
# # количество страниц не > 4000
# # цена не должна быть меньше 100 и больше 10000

class PageNumberError(Exception):
    message = 'Page number should not be more then 4000'

    def __init__(self):
        super().__init__(self.message)


class YearError(Exception):
    message = 'Year should not be less then 1980'

    def __init__(self):
        super().__init__(self.message)


class AuthorNameError(Exception):
    message = 'Author name should have only letters'

    def __init__(self):
        super().__init__(self.message)


class PriceError(Exception):
    message = 'Price should be between 100 and 10000'

    def __init__(self):
        super().__init__(self.message)


class Book:

    def __init__(self, author_name, year, pages, price):
        self.author_name = self.validate_author_name(author_name)
        self.year = self.validate_year(year)
        self.pages = self.validate_pages(pages)
        self.price = self.validate_price(price)

    @staticmethod
    def validate_author_name(name):
        if name.isalpha():
            return name
        else:
            raise AuthorNameError

    @staticmethod
    def validate_year(year):
        if year >= 1980:
            return year
        else:
            raise YearError

    @staticmethod
    def validate_pages(pages):
        if pages <= 4000:
            return pages
        else:
            raise PageNumberError

    @staticmethod
    def validate_price(price):
        if 100 <= price <= 10000:
            return price
        else:
            raise PriceError
