import string


def check_special_symbols(some_symbol):
    _word = set(some_symbol)
    punctuation_set = set(string.punctuation)
    if _word.intersection(punctuation_set):
        return True
    return False
