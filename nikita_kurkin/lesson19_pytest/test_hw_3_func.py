import pytest
from func_3 import check_special_symbols


class TestCheckSpecialSymbols:
    @pytest.mark.parametrize("s_character, expected_result", [
        ('!', True),
        ('$', True),
        ('1', False)
    ])
    def test_check_ss(self, s_character, expected_result):
        assert check_special_symbols(s_character) == expected_result
