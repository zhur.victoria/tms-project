from abc import ABC, abstractmethod


# Создать абстрактным класс Pet с абстрактным методом move(). Унаследуйте от
# класса Pet класс Parrot и переопределите в нем метод move().

class Pet(ABC):
    @abstractmethod
    def move(self):
        print('Pet can move')


class Parrot(Pet):
    def move(self):
        print('Parrot can move')


kesha = Parrot()
kesha.move()


# Task_2

class Animal(ABC):
    @abstractmethod
    def go(self):
        print('go go go')

    @abstractmethod
    def sleep(self):
        print('wanna sleep ZZzz')

    @abstractmethod
    def eat(self):
        print('fresh meat')

    @abstractmethod
    def drink(self):
        print('drink some water')


class Pet(Animal):
    @abstractmethod
    def love(self):
        print('only love can save me now')

    @abstractmethod
    def eat_slippers(self):
        print('mmm tasty')


class Wild_animal(Animal):
    @abstractmethod
    def hunt(self):
        print('ill find and kill ya')

    @abstractmethod
    def roar(self):
        print('rrrrrrrr')

    @abstractmethod
    def dig_a_hole(self):
        print('dig it deaper!')


# dog=Wild_animal()
# dog.drink()

class Dog(Pet):

    def go(self):
        print('go go go')

    def sleep(self):
        print('wanna sleep ZZzz')

    def eat(self):
        print('fresh meat')

    def drink(self):
        print('drink some water')

    def love(self):
        print('only love can save me now')

    def eat_slippers(self):
        print('mmm tasty')


class Cat(Pet):

    def go(self):
        print('go go go')

    def sleep(self):
        print('wanna sleep ZZzz')

    def eat(self):
        print('fresh meat')

    def drink(self):
        print('drink some water')

    def love(self):
        print('only love can save me now')

    def eat_slippers(self):
        print('mmm tasty')


class Lion(Wild_animal):

    def hunt(self):
        print('ill find and kill ya')

    def roar(self):
        print('rrrrrrrr')

    def dig_a_hole(self):
        print('dig it deaper!')

    def go(self):
        print('go go go')

    def sleep(self):
        print('wanna sleep ZZzz')

    def eat(self):
        print('fresh meat')

    def drink(self):
        print('drink some water')


class Wolf(Wild_animal):

    def hunt(self):
        print('ill find and kill ya')

    def roar(self):
        print('rrrrrrrr')

    def dig_a_hole(self):
        print('dig it deaper!')

    def go(self):
        print('go go go')

    def sleep(self):
        print('wanna sleep ZZzz')

    def eat(self):
        print('fresh meat')

    def drink(self):
        print('drink some water')


# Создать класс Book. Атрибуты: количество страниц, год издания, автор,
# цена. Добавить валидацию в конструкторе на ввод корректных данных.
# Создать иерархию ошибок.

class MyException(Exception):
    def __init__(self, message='default error'):
        super().__init__(message)


class Book:
    def __init__(self, pages, year, author, price):
        self.year = year
        self.pages = pages
        self.author = author
        self.price = price
        if self.year > 1980:
            raise MyException('Year is not correct')
        if self.pages < 4000:
            raise MyException('Pages input is  not correct')
        if not self.author.isalpha():
            raise MyException('Author is not correct')
        if self.price < 100 or self.price > 10000:
            raise MyException('Price is not correct')


carrol = Book(4005, 1976, 'Carrol', 9000)

# hello vasya----------------flake8 wants lambda to be changed to def
# y = lambda x: (f'hello {x}')
# print(y('vasya'))

# ['hello vasya', 'hello lizzy', 'hello koko']
names = ['vasya', 'lizzy', 'koko']
# names_lambda = lambda names: [f'hello {x}' for x in names]
# print(names_lambda(names))

# ['hello vasya', 'hello lizzy', 'hello koko'] in another way.
names_lambda = map(lambda x: f'hello {x}', names)
print(list(names_lambda))

# ['1', '2', '3', '4', '5'] from the list with integers
numbers_list = [1, 2, 3, 4, 5]
print(list(map(lambda x: str(x), numbers_list)))

# print names with 'k' letter:
print(list(filter(lambda names: [x for x in names if 'k' in x], names)))

# sort the dict by values using lambda and sorted method:
s = {"a": 10, "b": 20, "c": 0, "d": -1}
print(dict(sorted(s.items(), key=lambda el: el[1])))
