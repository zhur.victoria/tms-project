class InputTypeError(Exception):
    def __init__(self, message='Error!'):
        super().__init__(message)


def to_float_converter(arg):
    """
    This function checks the arg, converts it to float type or
    raises exception that arg is unable to convert.
    """
    if isinstance(arg, int):
        return float(arg)
    if isinstance(arg, str):
        if arg.isdigit():
            return float(arg)
        else:
            raise InputTypeError('Unable to convert!')
    else:
        raise InputTypeError('Unable to convert!')
