# количество страниц > 4000
# год < 1980
# автор не должен содержать других символов кроме букв
# цена не должна быть меньше 100 и больше 10000


class AuthorException(Exception):
    def __init__(self, message='Author can contain only letters'):
        super().__init__(message)


class YearException(Exception):
    def __init__(self, message='Year should be more than 1980'):
        super().__init__(message)


class PagesException(Exception):
    def __init__(self, message='Number of pages should be less than 4000'):
        super().__init__(message)


class PriceException(Exception):
    def __init__(self, message='Price should be in range 100 - 10000'):
        super().__init__(message)


class Book:
    def __init__(self, author, year, pages, price):
        self.author = self.validate_author(author)
        self.year = self.validate_year(year)
        self.pages = self.validate_pages(pages)
        self.price = self.validate_price(price)

    @staticmethod
    def validate_author(author):
        if author.isalpha():
            return author
        raise AuthorException

    @staticmethod
    def validate_year(year):
        if year > 1980:
            return year
        raise YearException

    @staticmethod
    def validate_pages(pages):
        if pages < 4000:
            return pages
        raise PagesException

    @staticmethod
    def validate_price(price):
        if 99 < price < 10001:
            return price
        raise PriceException
