import pytest
import datetime
import time


@pytest.fixture(scope="module", autouse=True)
def calculate_time_execution():
    today = datetime.datetime.today()
    start_module = time.time()
    print(f'Start time of tests execution is'
          f' {today.strftime("%Y-%m-%d-%H.%M.%S.%fZ")}')
    yield start_module
    today1 = datetime.datetime.today()
    print(f'\nEnd time of tests execution is'
          f' {today1.strftime("%Y-%m-%d-%H.%M.%S.%fZ")}')


@pytest.fixture(scope='module', autouse=True)
def print_file_name(request):
    print(f'\nAll tests are executed from {request.node.name} module')


@pytest.fixture(autouse=True)
def print_fixtures(calculate_time_execution, request):
    print(f'Fixtures used by this test are {request.fixturenames}')
    start_test = time.time()
    print('The difference between start of module and this tests  is ',
          start_test - calculate_time_execution)
