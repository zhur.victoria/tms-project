# Task1: Из заданной строки получить нужную строку:
# "String" => "SSttrriinngg"
# "Hello World" => "HHeelllloo  WWoorrlldd"
# "1234!_ " => "11223344!!__  "
str1 = '1234!_ '
new = ''
for i in str1:
    i = i * 2
    new = new + i
print('Task 1:', new)

# Task2: Если х в квадрате больше 1000 - пишем "Hot" иначе - "Nope"
# '50' == "Hot"
# 4 == "Nope"
# "12" == "Nope"
# 60 == "Hot"
x = 60
if x ** 2 > 1000:
    print('Task 2: Hot')
else:
    print('Task 2: Nope')

# Task 3: Сложите все числа в списке, они могут быть отрицательными,
# если список пустой вернуть 0
# [] == 0
# [1, 2, 3] == 6
# [1.1, 2.2, 3.3] == 6.6
# [4, 5, 6] == 15
# range(101) == 5050
lst1 = [1, 2, 3]
if lst1:
    print(sum(lst1))
else:
    print(0)

# Task 4: Подсчет букв Дано строка и буква => "fizbbbuz","b", нужно подсчитать
# сколько раз буква b встречается в заданной строке 3
# "Hello there", "e" == 3
# "Hello there", "t" == 1
# "Hello there", "h" == 2
# "Hello there", "L" == 2
# "Hello there", " " == 1
str2 = 'fizbbbuz'
letter = 'b'
counter = 0
for i in str2:
    if i == letter:
        counter += 1
print('Task 4:', counter)

# Task 5: Напишите код, который возьмет список строк и пронумерует их.
# Нумерация начинается с 1, имеет : и пробел
# [] => []
# ["a", "b", "c"] => ["1: a", "2: b", "3: c"]
lst2 = ["a", "b", "c"]
lst3 = []
number = 0
for i in lst2:
    number += 1
    lst3.append(str(number) + ': ' + i)
print('Task 5:', lst3)

# Task 6: Напишите программу, которая по данному числу n от 1 до n выводит на
# экран n флагов. Изображение одного флага имеет размер 4×4 символов. Внутри
# каждого флага должен быть записан его номер — число от 1 до n.
n = int(input('Task 6: Введите n: '))
for i in range(1, n + 1):
    print('+__')
    print('|' + str(i), '/')
    print('|__\\')
    print('|')

# Task 7: Напишите программу. Есть 2 переменные salary и bonus.
# Salary - integer, bonus - boolean.
# Если bonus - true, salary должна быть умножена на 10. Если false - нет
# 10000, True == '$100000'
# 25000, True == '$250000'
# 10000, False == '$10000'
# 60000, False == '$60000'
# 2, True == '$20'
# 78, False == '$78'
# 67890, True == '$678900'
salary = 40
bonus = True
if bonus:
    print('Task 7: $' + str(salary * 10))
else:
    print('task 7; $' + str(salary))

# Task 8: Проверить, все ли элементы одинаковые
# [1, 1, 1] == True
# [1, 2, 1] == False
# ['a', 'a', 'a'] == True
# [] == True
lst4 = ['a', 'a', 'a']
if len(lst4) == 0:
    print('Task 8:', True)
elif len(set(lst4)) == 1:
    print('Task 8:', True)
else:
    print('Task 8:', False)

# Task 9: Суммирование. Необходимо подсчитать сумму всех чисел до заданного:
# дано число 2, результат будет -> 3(1+2)
# дано число 8, результат будет -> 36(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8)
# 1 == 1
# 8 == 36
# 22 == 253
# 100 == 5050
k = 22
result = 0
for i in range(1, k + 1):
    result += i
print('Task 9:', result)

# Task 10: Проверка строки. В данной подстроке проверить все ли буквы в
# строчном регистре или нет и вернуть список не подходящих.
# dogcat => [True, []]
# doGCat => [False, ['G', 'C']]
str6 = 'doGCat'
str_not_lower = [False]
str_lower = [True]
unfit = []
if str6 == str6.lower():
    str_lower.append([])
    print('Task 10:', str_lower)
else:
    for i in str6:
        if i != i.lower():
            unfit.append(i)
    str_not_lower.append(unfit)
    print('Task 10:', str_not_lower)
