# Task 2: Банковский вклад
# Создайте класс инвестиция. Который содержит необходимые поля и методы,
# например сумма инвестиция и его срок. Пользователь делает инвестиция в
# размере N рублей сроком на R лет под 10% годовых (инвестиция с возможностью
# ежемесячной капитализации - это означает, что проценты прибавляются к сумме
# инвестиции ежемесячно).
# Написать класс Bank, метод deposit принимает аргументы N и R, и возвращает
# сумму, которая будет на счету пользователя.


class Investment:
    def __init__(self, amount, years):
        self.amount = amount
        self.years = years


invest1 = Investment(1000, 10)


class Bank:

    @staticmethod
    def deposit(investment):
        return investment.amount * (1 + (0.1 / 12)) ** (investment.years * 12)


print(Bank.deposit(invest1))
