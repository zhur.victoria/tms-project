import configparser
import pytest
from gitlab_helper import GitLabHelper, SecondGitLabHelper


PROJECT_ID = "31350226"
SHA = "6ca13e8435457134cb6a0fa56f0e30854508d3db"


@pytest.fixture()
def auth_config():
    config = configparser.ConfigParser()
    config.read("configuration.ini")
    return config.get("db", "auth_token")


def test_create_new_branch(auth_config):
    api_client = GitLabHelper(auth_config)
    branches = api_client.get_branches(PROJECT_ID)
    new_branch_name = "test_ne_new_branch"

    # create branch
    new_branch = api_client.create_branch(PROJECT_ID, new_branch_name,
                                          branches[0]["name"])
    all_branches = api_client.get_branches(PROJECT_ID)

    # filter branches by a new one
    f_ = [branch for branch in all_branches if new_branch["name"] ==
          branch["name"]]
    assert f_, "Created branch is not found"


def test_second_create_new_branch(auth_config):
    api_client = SecondGitLabHelper(auth_config)
    branches = api_client.get_branches(PROJECT_ID)
    new_branch_name = "test_ne_new_branch"

    # create branch
    new_branch = api_client.create_branch(PROJECT_ID, new_branch_name,
                                          branches[0]["name"])
    all_branches = api_client.get_branches(PROJECT_ID)

    # filter branches by a new one
    f_ = [branch for branch in all_branches if new_branch["name"] ==
          branch["name"]]
    assert f_, "Created branch is not found"


def test_create_comment(auth_config):
    api_client = SecondGitLabHelper(auth_config)
    new_comment = "Hello!"

    # commit in commits
    commits = api_client.get_commits(PROJECT_ID)
    commit = api_client.get_commits_sha(PROJECT_ID, SHA)
    if commit in commits:
        assert True

    # create comment
    create_comment = api_client.post_commits_comments(PROJECT_ID, new_comment,
                                                      SHA)
    comments_commit = api_client.get_commits_comments(PROJECT_ID, SHA)
    assert [create_comment] == comments_commit
