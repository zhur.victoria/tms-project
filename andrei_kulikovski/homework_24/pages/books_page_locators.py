from selenium.webdriver.common.by import By


class BooksPageLocators:
    BOOKS_PAGE_SUB = (By.XPATH, '//*[@class="page-header action"]/h1')
    BOOK_ITEMS = (By.XPATH, '//*[@class="row"]/li')
    BOOK_TITLE = (By.CSS_SELECTOR, '.product_pod a[title]')
