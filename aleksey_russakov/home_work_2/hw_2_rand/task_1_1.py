# задаём a и b
a = 3
b = 5

# формулы
my_sum = a + b
diff = a - b
product = a * b

# вывод суммы, разности и произведения
print(f"сумма: {my_sum}")
print(f"разность: {diff}")
print(f"произведение: {product}")
