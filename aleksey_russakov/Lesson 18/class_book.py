# количество страниц > 4000
# год < 1980
# автор не должен содержать других символов кроме букв
# цена не должна быть < 100 и > 10 000

class PageNumberError(Exception):
    message = "Page number should not be more than 4000"

    def __init__(self):
        super().__init__(self.message)


class YearError(Exception):
    message = "Year should not be less than 1980"

    def __init__(self):
        super().__init__(self.message)


class AuthorNameError(Exception):
    message = "Author name should have only letters"

    def __init__(self):
        super().__init__(self.message)


class PriceError(Exception):
    message = "Price should between 100 and 10 000"

    def __init__(self):
        super().__init__(self.message)


class Book:

    def __init__(self, author_name, year, pages, price):
        self.author_name = self.validate_author_name(author_name)
        self.year = self.validate_year(year)
        self.pages = self.validate_pages(pages)
        self.price = self.validate_price(price)
        print("Book written by", self.author_name, "is created")

    @staticmethod
    def validate_author_name(name):
        if name.isalpha():
            return name
        raise AuthorNameError

    @staticmethod
    def validate_year(year):
        if year >= 1980:
            return year
        raise YearError

    @staticmethod
    def validate_pages(pages):
        if pages <= 4000:
            return pages
        raise PageNumberError

    @staticmethod
    def validate_price(price):
        if (price >= 100) and (price <= 1000):
            return price
        raise PriceError


if __name__ == "__main__":
    book1 = Book("Alex", 2011, 2000, 200)
