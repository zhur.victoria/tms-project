import yaml
import json
from pprint import pprint
"""
Работа с yaml файлом
* Напечатайте номер заказа
* Напечатайте адрес отправки
* Напечатайте описание посылки, ее стоимость и кол-во
* Сконвертируйте yaml файл в json
* Создайте свой yaml файл
"""


with open('order.yaml') as yaml_file:
    templates = yaml.safe_load(yaml_file)

print("Напечатайте номер заказа:")
pprint(templates["invoice"])

print("Напечатайте адрес отправки:")
templates_list = list(templates['bill-to']['address'].values())
print(''.join(str(item) for item in templates_list))

print("Напечатайте описание посылки, ее стоимость и кол-во:")
templates_list = templates['product']
for item in templates_list:
    print(item['quantity'], item['description'], item['price'])

# Сконвертируйте yaml файл в json
str_object = json.dumps(templates, sort_keys=True, default=str)
json_obj = json.loads(str_object)
with open('yaml_to_json.json', 'w') as f:
    json.dump(json_obj, f)
    # json.dump(templates, f)

# Создайте свой yaml файл
with open('yaml_create.yaml', 'w') as f:
    yaml.dump(templates, f)
