from abc import ABC, abstractmethod
# На ферме могут жить животные и птицы
# И у тех и у других есть два атрибута: имя и возраст
# При этом нужно сделать так, чтобы возраст нельзя было изменить извне
# напрямую, но при этом можно было бы получить его значение
# У каждого животного должен быть обязательный метод – ходить
# А для птиц – летать
# Также должны быть общие для всех классов обязательные методы –
# постареть - который увеличивает возраст на 1 и метод голос


class Animal(ABC):
    def __init__(self, name, age):
        self.name = name
        self._age = age

    def __getattr__(self, name):
        raise NotExistException(f'{self.__class__.__name__} has no '
                                f'attribute {name}')

    @property
    def age(self):
        return self._age

    @abstractmethod
    def move(self):
        print('Animal can move')

    @abstractmethod
    def voice(self):
        print('Animal can roar')

    def get_older(self):
        self._age += 1


class Bird(Animal):
    def __init__(self, name, age):
        self.name = name
        self._age = age

    @property
    def age(self):
        return self._age


class Pig(Animal):
    def __str__(self):
        return f'{self.name}, {Pig.__name__}'

    def move(self):
        print('Pig can move')

    def voice(self):
        print('Pig can grunt')


class Cow(Animal):
    def __str__(self):
        return f'{self.name}, {Cow.__name__}'

    def move(self):
        print('Cow can move')

    def voice(self):
        print('Cow can moo')


class Goose(Bird):
    def __str__(self):
        return f'{self.name}, {Goose.__name__}'

    def move(self):
        print('Goose can fly')

    def voice(self):
        print('Goose can quack')


class Chicken(Bird):
    def __str__(self):
        return f'{self.name}, {Chicken.__name__}'

    def move(self):
        print('Chicken can fly')

    def voice(self):
        print('Chicken can cluck')


# После этого создать класс Ферма, в который можно передавать список
# животных. Должна быть возможность получить каждое животное, живущее
# на ферме как по индексу, так и через цикл for.Также у фермы должен быть
# метод, который увеличивает возраст всех животных, живущих на ней.
class Farm:
    def __init__(self, *beasts):
        self.beasts = beasts

    def __getitem__(self, index):
        return self.beasts[index]

    def __iter__(self):
        return iter(self.beasts)

    def get_older(self, *args):
        for beast in args:
            beast._age += 1


cow_1 = Cow('cow_1', 10)
pig_1 = Pig('pig_1', 11)
goose_1 = Goose('goose_1', 5)
chicken_1 = Chicken('chicken_1', 6)

farm_1 = Farm(cow_1, pig_1, goose_1, chicken_1)
print(farm_1[0])
for beasts in farm_1:
    print(beasts)
farm_1.get_older(cow_1, pig_1, goose_1, chicken_1)
print('cow_1', cow_1.age)
print(pig_1.age)


# При обращении к несуществующему методу или атрибуту животного или птицы
# должен срабатывать пользовательский exception NotExistException
# и выводить сообщение о том, что для текущего класса (имя класса),
# такой метод или атрибут не существует – (имя метода или атрибута)
class NotExistException(Exception):
    pass


pig = Pig('pig_1', 10)
print(pig.age)
print(pig.name)
print(chicken_1.pig())
