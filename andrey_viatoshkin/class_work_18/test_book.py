import unittest
from unittest.mock import patch

from tms.andrey_viatoshkin.class_work_11.class_work_11\
    import Book, PagesException, PriceException, YearException


def mock_author_validation(arg):
    return 'Толкиен'


class TestBookPages(unittest.TestCase):

    def test_validate_pages_with_pages_less_than_four_thousand(self):
        result = Book.validate_pages(3999)
        self.assertEqual(result, 3999)

    def test_validate_pages_with_pages_more_than_four_thousand(self):
        with self.assertRaises(PagesException):
            Book.validate_pages(4000)

    def test_validate_pages_with_pages_equals_to_none(self):
        with self.assertRaises(TypeError):
            Book.validate_pages()

    def test_validate_pages_with_pages_type_equals_to_str(self):
        with self.assertRaises(TypeError):
            Book.validate_pages("3000")


class TestBookYear(unittest.TestCase):

    def test_validate_year_with_year_greater_than_1980(self):
        result = Book.validate_year(1981)
        self.assertEqual(result, 1981)

    def test_validate_year_with_year_less_1981(self):
        with self.assertRaises(YearException):
            Book.validate_year(1980)

    def test_validate_pages_with_year_equals_to_none(self):
        with self.assertRaises(TypeError):
            Book.validate_year()

    def test_validate_pages_with_year_type_equals_to_str(self):
        with self.assertRaises(TypeError):
            Book.validate_year("3000")


class TestBookPrice(unittest.TestCase):

    def test_validate_price_inbetween_100_and_10000(self):
        result = Book.validate_price(101)
        self.assertEqual(result, 101)

    def test_validate_price_equals_to_100(self):
        with self.assertRaises(PriceException):
            Book.validate_price(100)

    def test_validate_price_equals_to_10001(self):
        with self.assertRaises(PriceException):
            Book.validate_price(10001)

    def test_validate_price_equals_to_none(self):
        with self.assertRaises(TypeError):
            Book.validate_price()

    def test_validate_pages_with_price_type_equals_to_str(self):
        with self.assertRaises(TypeError):
            Book.validate_price("3000")


class TestBookAuthor(unittest.TestCase):

    @patch('test_book.Book.validate_author',
           side_effect=mock_author_validation)
    def test_validate_author_mock(self, arg):
        author = Book(3999, 2000, 1, 9999)
        self.assertIsInstance(author, Book)
