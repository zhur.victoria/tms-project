# 1 У класса есть конструктор, в который надо передать имя и ЗП.
# У класса есть 2 метода: получение имени сотрудника и получение количества
# созданных работников. При создании экземпляра класса должен вызываться
# метод, который отвечает за увеличение количества работников на 1

class Employee:
    number_of_employees = 0

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
        Employee.increase_by_1()

    @classmethod
    def return_quantity_of_empl(cls):
        return cls.number_of_employees

    @classmethod
    def increase_by_1(cls):
        cls.number_of_employees += 1

    def get_name(self):
        print(self.name)

    def get_quantity_of_employees(self):
        print(self.number_of_employees)


empl_1 = Employee('Andrey', 100)
print(Employee.number_of_employees)
empl_2 = Employee('Andrey', 100)
print(Employee.number_of_employees)
empl_3 = Employee('Andrey', 100)
print(Employee.number_of_employees)

# 2 Вы идете в путешествие, надо подсчитать сколько у денег у каждого
# студента. Необходимо понять у кого больше всего денег и вернуть его имя.
# Если у студентов денег поровну вернуть: “all”.


class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money

    def get_name(self):
        return self.name

    def get_money(self):
        return self.money


student_1 = Student('Ivan', 10)
student_2 = Student('Oleg', 10)
student_3 = Student('Sergei', 11)


def rich_student(arg):
    student_money = [student.money for student in arg]
    max_money = max(student_money)
    if student_money.count(max_money) == len(student_money):
        return 'all'
    for student in arg:
        if student.money == max_money:
            return student.name


assert rich_student([student_1, student_2, student_3]) == 'Sergei'
assert rich_student([student_1, student_2]) == 'all'
assert rich_student([student_1, student_3]) == 'all'
